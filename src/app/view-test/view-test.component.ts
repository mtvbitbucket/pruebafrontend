import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Test} from "../Shared/Models/test";
import {User} from "../Shared/Models/User";

@Component({
  selector: 'app-view-test',
  templateUrl: './view-test.component.html',
  styleUrls: ['./view-test.component.css']
})
export class ViewTestComponent implements OnInit {

  @Input()
  test: Test;

  @Output()
  startTest = new EventEmitter<number>();

  @Output()
  stopTest = new EventEmitter<number>();

  constructor() {

  }

  ngOnInit() {
    if (this.test) {
      console.log(this.test);
    }
    else {
      console.log("No test found, something went wrong!")
    }
  }

  testStart() {
    this.startTest.emit(this.test.id);
  }

  testStop() {
    this.stopTest.emit(this.test.id);
  }

}
