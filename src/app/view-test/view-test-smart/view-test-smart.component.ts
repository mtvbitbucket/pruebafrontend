import { Component, OnInit } from '@angular/core';
import {Test} from "../../Shared/Models/test";
import {ActivatedRoute, Router} from "@angular/router";
import {TestService} from "../../Shared/Services/test.service";

@Component({
  selector: 'app-view-test-smart',
  templateUrl: './view-test-smart.component.html',
  styleUrls: ['./view-test-smart.component.css']
})
export class ViewTestSmartComponent implements OnInit {

  private id: number;

  private test: Test;

  constructor(private route: ActivatedRoute, private testService: TestService, private router: Router) {
    this.id = +route.snapshot.paramMap.get('id');

    this.testService.getTest(this.id).subscribe(data => {
      this.test = data;
    });
  }

  ngOnInit() {
  }

  startTest(event) {
    this.testService.startTheTest(event);
  }

  stopTest(event) {
    this.testService.stopTheTest(event);
  }

}
