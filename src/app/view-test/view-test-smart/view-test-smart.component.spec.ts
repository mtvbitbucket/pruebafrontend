import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewTestSmartComponent } from './view-test-smart.component';

describe('ViewTestSmartComponent', () => {
  let component: ViewTestSmartComponent;
  let fixture: ComponentFixture<ViewTestSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewTestSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewTestSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
