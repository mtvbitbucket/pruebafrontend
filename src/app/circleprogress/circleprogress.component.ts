import {Component, OnInit} from '@angular/core';
import {RoundProgressConfig} from "angular-svg-round-progressbar";

@Component({
  selector: 'app-circleprogress',
  templateUrl: './circleprogress.component.html',
  styleUrls: ['./circleprogress.component.css']
})
export class CircleprogressComponent implements OnInit{


  private current: number;

  options = {
    minimum: 0.02,
    maximum: 1,
    ease: 'linear',
    speed: 200,
    trickleSpeed: 300,
    meteor: true,
    spinner: false,
    spinnerPosition: 'right',
    direction: 'leftToRightIncreased',
    color: 'orange',
    thick: true
  };

  constructor(private _config: RoundProgressConfig) {
    _config.setDefaults({
      color: '#F1BE48',
      background: '#333'
    });
  }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.current = 41;
  }
}

