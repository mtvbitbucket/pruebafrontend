import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';
import { RetailerComponent } from './retailer/retailer.component';
import { LoginComponent } from './login/login.component';
import { AdminCreateUserComponent } from './admin/admin-create-user/admin-create-user.component';
import { LandingPageComponent } from './landing-page/landing-page.component';
import { AuthenticationService } from './Shared/Services/AuthenticationService';
import { UserService } from './Shared/Services/UserService';
import { TokenInterceptor } from './Shared/Interceptors/TokenInterceptor';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { AdminComponent } from './admin/admin.component';
import { RoundProgressModule } from 'angular-svg-round-progressbar';
import { AuthGuard } from './Shared/Guards/AuthGuard';
import { AdminGuard } from './Shared/Guards/AdminGuard';
import { TokenManager } from './Shared/TokenManager';
import { RequestInterceptor } from './Shared/Interceptors/RequestInterceptor';
import { NgProgressHttpClientModule } from '@ngx-progressbar/http-client';
import { NgProgressModule } from '@ngx-progressbar/core';
import { APP_BASE_HREF } from '@angular/common';
import { UserlistComponent } from './userlist/userlist.component';
import { UserlistSmartComponent } from './userlist/userlist-smart/userlist-smart.component';
import { TestlistComponent } from './testlist/testlist.component';
import { TestlistSmartComponent } from './testlist/testlist-smart/testlist-smart.component';
import { CircleprogressComponent } from './circleprogress/circleprogress.component';
import { LoadingbarComponent } from './loadingbar/loadingbar.component';
import { AdminUpdateuserComponent } from './admin/admin-updateuser/admin-updateuser.component';
import { RetailerSmartComponent } from './retailer/retailer-smart/retailer-smart.component';
import { CreateTestComponent } from './create-test/create-test.component';
import { CreateTestSmartComponent } from './create-test/create-test-smart/create-test-smart.component';

import { AdminUpdateuserSmartComponent } from './admin/admin-updateuser/admin-updateuser-smart/admin-updateuser-smart.component';
import { AdminDeleteUserComponent } from './admin/admin-delete-user/admin-delete-user.component';
import { AdminDeleteUserSmartComponent } from './admin/admin-delete-user/admin-delete-user-smart/admin-delete-user-smart.component';
import { AdminViewuserComponent } from './admin/admin-viewuser/admin-viewuser.component';
import { AdminViewuserSmartComponent } from './admin/admin-viewuser/admin-viewuser-smart/admin-viewuser-smart.component';
import {TestService} from "./Shared/Services/test.service";
import { AdminAssignrolesComponent } from './admin/admin-assignroles/admin-assignroles.component';
import { AdminViewtestsComponent } from './admin/admin-viewtests/admin-viewtests.component';
import { AdminViewtestsSmartComponent } from './admin/admin-viewtests/admin-viewtests-smart/admin-viewtests-smart.component';
import { AdminAssignrolesSmartComponent } from './admin/admin-assignroles/admin-assignroles-smart/admin-assignroles-smart.component';
import {RetailerCreateuserComponent} from "./retailer/retailer-createuser/retailer-createuser.component";
import {RetailerCreateuserSmartComponent} from "./retailer/retailer-createuser/retailer-createuser-smart/retailer-createuser-smart.component";
import {RetailerUpdateuserSmartComponent} from "./retailer/retailer-updateuser/retailer-updateuser-smart/retailer-updateuser-smart.component";
import {RetailerViewtestsComponent} from "./retailer/retailer-viewtests/retailer-viewtests.component";
import {RetailerViewtestsSmartComponent} from "./retailer/retailer-viewtests/retailer-viewtests-smart/retailer-viewtests-smart.component";
import {RetailerViewuserComponent} from "./retailer/retailer-viewuser/retailer-viewuser.component";
import {RetailerViewuserSmartComponent} from "./retailer/retailer-viewuser/retailer-viewuser-smart/retailer-viewuser-smart.component";
import {AdminSmartComponent} from "./admin/admin-smart/admin-smart.component";
import {RetailerUpdateuserComponent} from "./retailer/retailer-updateuser/retailer-updateuser.component";
import {AdminViewTemplatesComponent} from "./admin/admin-view-templates/admin-view-templates.component";
import {AdminViewTemplatesSmartComponent} from "./admin/admin-view-templates/admin-view-templates-smart/admin-view-templates-smart.component";
import {MailtemplateTableComponent} from "./admin/mailtemplate-table/mailtemplate-table.component";
import {MailtemplateRowComponent} from "./admin/mailtemplate-table/mailtemplate-row/mailtemplate-row.component";
import {AdminCreateTemplateSmartComponent} from "./admin/admin-create-template/admin-create-template-smart/admin-create-template-smart.component";
import {AdminCreateTemplateComponent} from "./admin/admin-create-template/admin-create-template.component";
import {AdminEditTemplateComponent} from "./admin/admin-edit-template/admin-edit-template.component";
import {AdminEditTemplateSmartComponent} from "./admin/admin-edit-template/admin-edit-template-smart/admin-edit-template-smart.component";
import {MailTemplateService} from "./Shared/Services/MailTemplateService";
import {RoleService} from "./Shared/Services/RoleService";
import {ViewTestComponent} from "./view-test/view-test.component";
import {ViewTestSmartComponent} from "./view-test/view-test-smart/view-test-smart.component";
import { EditTestComponent } from './edit-test/edit-test.component';
import { EditTestSmartComponent } from './edit-test/edit-test-smart/edit-test-smart.component';
import { AdminCreateUserSmartComponent } from './admin/admin-create-user/admin-create-user-smart/admin-create-user-smart.component';


@NgModule({
  declarations: [
    AppComponent,
    RetailerComponent,
    LoginComponent,
    AdminComponent,
    AdminCreateUserComponent,
    LandingPageComponent,
    UserlistComponent,
    UserlistSmartComponent,
    TestlistComponent,
    TestlistSmartComponent,
    CircleprogressComponent,
    LoadingbarComponent,
    AdminUpdateuserComponent,
    RetailerSmartComponent,
    CreateTestComponent,
    CreateTestSmartComponent,
    AdminUpdateuserSmartComponent,
    AdminUpdateuserComponent,
    AdminDeleteUserComponent,
    AdminDeleteUserSmartComponent,
    AdminViewuserComponent,
    AdminViewuserSmartComponent,
    AdminAssignrolesComponent,
    AdminViewtestsComponent,
    AdminViewtestsSmartComponent,
    AdminAssignrolesSmartComponent,
    RetailerCreateuserComponent,
    RetailerCreateuserSmartComponent,
    RetailerUpdateuserSmartComponent,
    RetailerViewtestsComponent,
    RetailerViewtestsSmartComponent,
    RetailerViewuserComponent,
    RetailerViewuserSmartComponent,
    AdminSmartComponent,
    RetailerUpdateuserComponent,
    AdminViewTemplatesComponent,
    AdminViewTemplatesSmartComponent,
    MailtemplateTableComponent,
    MailtemplateRowComponent,
    AdminCreateTemplateComponent,
    AdminCreateTemplateSmartComponent,
    AdminEditTemplateComponent,
    AdminEditTemplateSmartComponent,
    ViewTestComponent,
    ViewTestSmartComponent,
    //EditTestComponent,
    //EditTestSmartComponent,
    AdminCreateUserSmartComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    [RoundProgressModule],
    NgProgressHttpClientModule,
    NgProgressModule.forRoot()
  ],
  providers: [
    { provide: APP_BASE_HREF, useValue: '/'},
    AuthenticationService,
    UserService,
    TestService,
    RoleService,
    MailTemplateService,
    AuthGuard,
    AdminGuard,
    TokenManager,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
