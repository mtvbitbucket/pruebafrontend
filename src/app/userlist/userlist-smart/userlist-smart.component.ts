import {Component, Input, OnInit} from '@angular/core';
import { UserService } from '../../Shared/Services/UserService';
import { User } from '../../Shared/Models/User';


@Component({
  selector: 'app-userlist-smart',
  templateUrl: './userlist-smart.component.html',
  styleUrls: ['./userlist-smart.component.css']
})
export class UserlistSmartComponent {

  @Input()
  simplified: boolean;

  @Input()
  users: User[];

  constructor() { }

    ngOnInit() { }

}
