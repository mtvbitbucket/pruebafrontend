import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UserlistSmartComponent } from './userlist-smart.component';

describe('UserlistSmartComponent', () => {
  let component: UserlistSmartComponent;
  let fixture: ComponentFixture<UserlistSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UserlistSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UserlistSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
