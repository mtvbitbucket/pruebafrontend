import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../Shared/Models/User";

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {

  @Input()
  simplified: boolean;
  @Input()
  users: User[];

  constructor() { }

  ngOnInit() {
  }

}
