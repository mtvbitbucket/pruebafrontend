import { Component, OnInit } from '@angular/core';
import {RoundProgressConfig} from "angular-svg-round-progressbar";

@Component({
  selector: 'app-loadingbar',
  templateUrl: './loadingbar.component.html',
  styleUrls: ['./loadingbar.component.css']
})
export class LoadingbarComponent {

  options = {
    minimum: 0.02,
    maximum: 1,
    ease: 'linear',
    speed: 200,
    trickleSpeed: 300,
    meteor: true,
    spinner: false,
    spinnerPosition: 'right',
    direction: 'leftToRightIncreased',
    color: 'orange',
    thick: true
  };

  constructor(private _config: RoundProgressConfig) {
    _config.setDefaults({
      color: '#F1BE48',
      background: '#333'
    });
  }
}
