import { Component, OnInit } from '@angular/core';
import {UserService} from "../../Shared/Services/UserService";
import {TestService} from "../../Shared/Services/test.service";
import {Test} from "../../Shared/Models/test";
import {User} from "../../Shared/Models/User";

@Component({
  selector: 'app-admin-smart',
  templateUrl: './admin-smart.component.html',
  styleUrls: ['./admin-smart.component.css']
})
export class AdminSmartComponent implements OnInit {

  private tests: Test[];
  private users: User[];
  private simplified: boolean;

  constructor(private testService: TestService, private userService: UserService) { }

  ngOnInit() {
    this.simplified = true;
    this.retrieveUsers();
    this.retrieveTests();
  }

  retrieveUsers() {
    this.userService.getUsers().subscribe(users => {
      this.users = users;
    })
  }

  retrieveTests() {
    this.testService.getTestsAdmin().subscribe(tests => {
      this.tests = tests;
    })
  }
}
