import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminSmartComponent } from './admin-smart.component';

describe('AdminSmartComponent', () => {
  let component: AdminSmartComponent;
  let fixture: ComponentFixture<AdminSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
