import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewTemplatesComponent } from './admin-view-templates.component';

describe('AdminViewTemplatesComponent', () => {
  let component: AdminViewTemplatesComponent;
  let fixture: ComponentFixture<AdminViewTemplatesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewTemplatesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewTemplatesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
