import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewTemplatesSmartComponent } from './admin-view-templates-smart.component';

describe('AdminViewTemplatesSmartComponent', () => {
  let component: AdminViewTemplatesSmartComponent;
  let fixture: ComponentFixture<AdminViewTemplatesSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewTemplatesSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewTemplatesSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
