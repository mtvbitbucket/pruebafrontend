import { Component, OnInit } from '@angular/core';
import {MailTemplate} from '../../../Shared/Models/MailTemplate';
import {MailTemplateService} from '../../../Shared/Services/MailTemplateService';

@Component({
  selector: 'app-admin-view-templates-smart',
  templateUrl: './admin-view-templates-smart.component.html',
  styleUrls: ['./admin-view-templates-smart.component.css']
})
export class AdminViewTemplatesSmartComponent implements OnInit {

  private templates: MailTemplate[];

  constructor(private service: MailTemplateService) {

  }

  ngOnInit() {
    this.service.getAll().subscribe(data => {
      this.templates = data;
    }, error => {
      console.log(error);
    });
  }
}
