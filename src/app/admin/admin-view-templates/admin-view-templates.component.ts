import {Component, Input, OnInit} from '@angular/core';
import {MailTemplate} from '../../Shared/Models/MailTemplate';

@Component({
  selector: 'app-admin-view-templates',
  templateUrl: './admin-view-templates.component.html',
  styleUrls: ['./admin-view-templates.component.css']
})
export class AdminViewTemplatesComponent implements OnInit {

  @Input()
  templates: MailTemplate[];

  constructor() {
  }

  ngOnInit() {
  }
}
