import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCreateTemplateComponent } from './admin-create-template.component';

describe('AdminCreateTemplateComponent', () => {
  let component: AdminCreateTemplateComponent;
  let fixture: ComponentFixture<AdminCreateTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCreateTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCreateTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
