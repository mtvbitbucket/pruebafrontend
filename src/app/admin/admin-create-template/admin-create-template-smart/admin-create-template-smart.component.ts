import { Component, OnInit } from '@angular/core';
import {MailTemplate} from '../../../Shared/Models/MailTemplate';
import {MailTemplateService} from '../../../Shared/Services/MailTemplateService';

@Component({
  selector: 'app-admin-create-template-smart',
  templateUrl: './admin-create-template-smart.component.html',
  styleUrls: ['./admin-create-template-smart.component.css']
})
export class AdminCreateTemplateSmartComponent implements OnInit {

  constructor(private service: MailTemplateService) { }

  ngOnInit() {
  }

  async createTemplate(template: MailTemplate) {
    await console.log("Smart-create");
    this.service.create(template).subscribe(data => {
      console.log(data);
    }, error => {
      console.log(error);
    });
  }

}
