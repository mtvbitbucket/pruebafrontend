import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCreateTemplateSmartComponent } from './admin-create-template-smart.component';

describe('AdminCreateTemplateSmartComponent', () => {
  let component: AdminCreateTemplateSmartComponent;
  let fixture: ComponentFixture<AdminCreateTemplateSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCreateTemplateSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCreateTemplateSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
