import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {MailTemplate} from '../../Shared/Models/MailTemplate';

@Component({
  selector: 'app-admin-create-template',
  templateUrl: './admin-create-template.component.html',
  styleUrls: ['./admin-create-template.component.css']
})
export class AdminCreateTemplateComponent implements OnInit {

  @Output()
  emitter = new EventEmitter<MailTemplate>();

  model: any = {};
  constructor() { }

  ngOnInit() {
  }

  createTemplate() {
    this.emitter.emit(this.model);
  }

}
