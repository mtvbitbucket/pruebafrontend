import {Component, Input, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../Shared/Services/UserService";
import {User} from "../../../Shared/Models/User";
import {EditUser} from "../../../Shared/Models/EditUser";

@Component({
  selector: 'app-admin-updateuser-smart',
  templateUrl: './admin-updateuser-smart.component.html',
  styleUrls: ['./admin-updateuser-smart.component.css']
})
export class AdminUpdateuserSmartComponent implements OnInit {

  private id: string;

  private user: User;

  private updatesUser: any = {};


  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.id = route.snapshot.paramMap.get('id');

    this.userService.getUser(this.id).subscribe(data => {
      this.user = data;
    })
  }

  updateUser(event) {
    this.updatesUser = event;
    this.userService.editUser(this.updatesUser)
      .subscribe(
        data => {
          this.router.navigate(['/admin/users']);
          return data;
        },
        error => {
          console.log(error);
          return error;
        });
  }

  ngOnInit() {
  }

}
