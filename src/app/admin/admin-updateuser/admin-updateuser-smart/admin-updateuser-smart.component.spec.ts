import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUpdateuserSmartComponent } from './admin-updateuser-smart.component';

describe('AdminUpdateuserSmartComponent', () => {
  let component: AdminUpdateuserSmartComponent;
  let fixture: ComponentFixture<AdminUpdateuserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUpdateuserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUpdateuserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
