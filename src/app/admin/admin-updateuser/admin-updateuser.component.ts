import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../Shared/Models/User";
import {EditUser} from "../../Shared/Models/EditUser";

@Component({
  selector: 'app-admin-updateuser',
  templateUrl: './admin-updateuser.component.html',
  styleUrls: ['./admin-updateuser.component.css']
})
export class AdminUpdateuserComponent implements OnInit {

  @Input()
  user: User;

  @Output()
  updatedUser = new EventEmitter<EditUser>();

  model: any = {};

  constructor() {

  }

  ngOnInit() {
    if (this.user) {
      this.model = this.user;
      console.log("Below is model from admin-updateuser")
      console.log(this.model);
    }
  }

  updateUser() {
    console.log(this.model);
    this.updatedUser.emit(this.model);
  }
}
