import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminUpdateuserComponent } from './admin-updateuser.component';

describe('AdminUpdateuserComponent', () => {
  let component: AdminUpdateuserComponent;
  let fixture: ComponentFixture<AdminUpdateuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminUpdateuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminUpdateuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
