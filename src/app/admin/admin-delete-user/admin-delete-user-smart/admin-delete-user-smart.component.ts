import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../Shared/Services/UserService";

@Component({
  selector: 'app-admin-delete-user-smart',
  templateUrl: './admin-delete-user-smart.component.html',
  styleUrls: ['./admin-delete-user-smart.component.css']
})
export class AdminDeleteUserSmartComponent {

  private id: string;

  constructor(private router: Router, private route: ActivatedRoute, private userService: UserService) {
    this.id = this.route.snapshot.paramMap.get('id');
    if (window.confirm('Vil du virkelig slette denne bruger?')) {
      this.deleteUser();
    }
    else {
      router.navigate(['/admin/users'])
    }

  }

  async deleteUser() {
    await this.userService.deleteUser(this.id).subscribe(data => {
        if (data === true)
          this.router.navigate(['/admin/users']);
      },
      error => console.log(error)
    );
  }
}
