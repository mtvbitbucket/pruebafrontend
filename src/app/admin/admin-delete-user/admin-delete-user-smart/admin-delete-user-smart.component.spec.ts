import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminDeleteUserSmartComponent } from './admin-delete-user-smart.component';

describe('AdminDeleteUserSmartComponent', () => {
  let component: AdminDeleteUserSmartComponent;
  let fixture: ComponentFixture<AdminDeleteUserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminDeleteUserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminDeleteUserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
