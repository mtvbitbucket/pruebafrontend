import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewuserSmartComponent } from './admin-viewuser-smart.component';

describe('AdminViewuserSmartComponent', () => {
  let component: AdminViewuserSmartComponent;
  let fixture: ComponentFixture<AdminViewuserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewuserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewuserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
