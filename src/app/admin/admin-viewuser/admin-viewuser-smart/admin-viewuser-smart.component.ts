import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../Shared/Services/UserService";
import {User} from "../../../Shared/Models/User";

@Component({
  selector: 'app-admin-viewuser-smart',
  templateUrl: './admin-viewuser-smart.component.html',
  styleUrls: ['./admin-viewuser-smart.component.css']
})
export class AdminViewuserSmartComponent implements OnInit {

  private simplified: boolean;

  private users: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.simplified = false;
    this.retrieveAllUsers();
  }
  async retrieveAllUsers() {
    await this.userService.getUsers().subscribe(users => {
      this.users = users;
    });
  }



}
