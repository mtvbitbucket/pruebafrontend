import {Component, Input, OnInit} from '@angular/core';
import {User} from "../../Shared/Models/User";

@Component({
  selector: 'app-admin-viewuser',
  templateUrl: './admin-viewuser.component.html',
  styleUrls: ['./admin-viewuser.component.css']
})
export class AdminViewuserComponent implements OnInit {

  @Input()
  simplified: boolean;

  @Input()
  users: User[];

  constructor() { }

  ngOnInit() {
  }

}
