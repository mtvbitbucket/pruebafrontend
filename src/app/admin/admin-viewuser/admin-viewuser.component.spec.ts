import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewuserComponent } from './admin-viewuser.component';

describe('AdminViewuserComponent', () => {
  let component: AdminViewuserComponent;
  let fixture: ComponentFixture<AdminViewuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
