import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAssignrolesComponent } from './admin-assignroles.component';

describe('AdminAssignrolesComponent', () => {
  let component: AdminAssignrolesComponent;
  let fixture: ComponentFixture<AdminAssignrolesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAssignrolesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAssignrolesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
