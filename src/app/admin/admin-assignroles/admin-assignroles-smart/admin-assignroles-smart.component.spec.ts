import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAssignrolesSmartComponent } from './admin-assignroles-smart.component';

describe('AdminAssignrolesSmartComponent', () => {
  let component: AdminAssignrolesSmartComponent;
  let fixture: ComponentFixture<AdminAssignrolesSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAssignrolesSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAssignrolesSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
