import { Component, OnInit } from '@angular/core';
import {User} from "../../../Shared/Models/User";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../Shared/Services/UserService";
import {RoleService} from "../../../Shared/Services/RoleService";
import {Roles} from "../../../Shared/Models/roles";

@Component({
  selector: 'app-admin-assignroles-smart',
  templateUrl: './admin-assignroles-smart.component.html',
  styleUrls: ['./admin-assignroles-smart.component.css']
})
export class AdminAssignrolesSmartComponent implements OnInit {

  private id: string;

  private user: User;

  private model: any = {};

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router, private roleService: RoleService) {
    this.id = route.snapshot.paramMap.get('id');

    this.userService.getUser(this.id).subscribe(data => {
      this.user = data;
    });
  }


  ngOnInit() {

  }

  async assignRolesToUser(event) {
    this.model = event;

    console.log(this.model)

    await this.userService.assignRoles(this.id, this.model)
      .subscribe(
        data => {
          this.router.navigate(["/admin/users"])
          return data;
        },
        err => {
          return err;
        })
  }

}
