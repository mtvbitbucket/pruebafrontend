import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Roles} from "../../Shared/Models/roles";

@Component({
  selector: 'app-admin-assignroles',
  templateUrl: './admin-assignroles.component.html',
  styleUrls: ['./admin-assignroles.component.css']
})
export class AdminAssignrolesComponent implements OnInit {

  @Output()
  roles = new EventEmitter<string[]>();

  rolesToAssign: string[];

  private admin = "Admin";
  private retailer = "Retailer";
  private customer = "Customer";

  constructor() {
    this.rolesToAssign = [];
  }

  ngOnInit() {

  }

  assign() {
    let adminCheckbox = <HTMLInputElement> document.getElementById("admin");
    let retailerCheckbox = <HTMLInputElement> document.getElementById("retailer");
    let customerCheckbox = <HTMLInputElement> document.getElementById("customer");
    let adminChecked = adminCheckbox.checked;
    let retailerChecked = retailerCheckbox.checked;
    let customerChecked = customerCheckbox.checked;

    if(adminChecked)
    {
      this.rolesToAssign.push(this.admin);
    }
    if(retailerChecked)
    {
      this.rolesToAssign.push(this.retailer);
    }
    if(customerChecked)
    {
      this.rolesToAssign.push(this.customer);
    }

    console.log(this.rolesToAssign);
    this.roles.emit(this.rolesToAssign);
  }

}
