import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from '../../Shared/Models/User';
import {EditUser} from '../../Shared/Models/EditUser';
import {Template} from '../../Shared/Models/template';

@Component({
  selector: 'app-admin-edit-template',
  templateUrl: './admin-edit-template.component.html',
  styleUrls: ['./admin-edit-template.component.css']
})
export class AdminEditTemplateComponent implements OnInit {

  constructor() { }

  @Input()
  template: Template;

  @Output()
  emitter = new EventEmitter<Template>();

  model: any = {};
  ngOnInit() {
    if (!this.template) {
      return;
    }
    this.model = this.template;
  }

  updateTemplate() {
    this.emitter.emit(this.model);
  }

}
