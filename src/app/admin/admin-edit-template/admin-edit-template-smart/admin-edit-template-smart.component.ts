import { Component, OnInit } from '@angular/core';
import {MailTemplateService} from '../../../Shared/Services/MailTemplateService';
import {Template} from '../../../Shared/Models/template';
import {ActivatedRoute, Router} from '@angular/router';
import {MailTemplate} from '../../../Shared/Models/MailTemplate';

@Component({
  selector: 'app-admin-edit-template-smart',
  templateUrl: './admin-edit-template-smart.component.html',
  styleUrls: ['./admin-edit-template-smart.component.css']
})
export class AdminEditTemplateSmartComponent implements OnInit {

  private id: number;
  private template: MailTemplate;
  constructor(
    private route: ActivatedRoute,
    private service: MailTemplateService,
    private router: Router) {
    this.id = Number(route.snapshot.paramMap.get('id'));

    this.service.get(this.id).subscribe(data => {
      this.template = data;
    });
  }

  updateTemplate(event) {
    let updatedTemplate: MailTemplate;
    updatedTemplate = event;

    this.service.edit(updatedTemplate)
      .subscribe(
        data => {
          this.router.navigate(['/admin/templates']);
          return data;
        },
        error => {
          return error;
        });
  }

  ngOnInit() {

  }

}
