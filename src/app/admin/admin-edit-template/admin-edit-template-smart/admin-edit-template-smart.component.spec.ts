import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditTemplateSmartComponent } from './admin-edit-template-smart.component';

describe('AdminEditTemplateSmartComponent', () => {
  let component: AdminEditTemplateSmartComponent;
  let fixture: ComponentFixture<AdminEditTemplateSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditTemplateSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditTemplateSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
