import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminEditTemplateComponent } from './admin-edit-template.component';

describe('AdminEditTemplateComponent', () => {
  let component: AdminEditTemplateComponent;
  let fixture: ComponentFixture<AdminEditTemplateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminEditTemplateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminEditTemplateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
