import {Component, Input, OnInit} from '@angular/core';
import {Test} from "../Shared/Models/test";
import {User} from "../Shared/Models/User";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  @Input()
  users: User[];

  @Input()
  tests: Test[];

  @Input()
  simplified: boolean;

  constructor() {
  }

  ngOnInit() {

  }
}
