import {Component, Input, OnInit} from '@angular/core';
import {MailTemplate} from '../../../Shared/Models/MailTemplate';

@Component({
  selector: 'app-mailtemplate-row',
  templateUrl: './mailtemplate-row.component.html',
  styleUrls: ['./mailtemplate-row.component.css']
})
export class MailtemplateRowComponent implements OnInit {

  @Input()
  template: MailTemplate;

  constructor() { }

  ngOnInit() {
  }

}
