import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailtemplateRowComponent } from './mailtemplate-row.component';

describe('MailtemplateRowComponent', () => {
  let component: MailtemplateRowComponent;
  let fixture: ComponentFixture<MailtemplateRowComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailtemplateRowComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailtemplateRowComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
