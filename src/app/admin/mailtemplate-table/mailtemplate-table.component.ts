import {Component, Input, OnInit} from '@angular/core';
import {MailTemplate} from '../../Shared/Models/MailTemplate';

@Component({
  selector: 'app-mailtemplate-table',
  templateUrl: './mailtemplate-table.component.html',
  styleUrls: ['./mailtemplate-table.component.css']
})
export class MailtemplateTableComponent implements OnInit {

  @Input()
  templates: MailTemplate[];

  constructor() { }

  ngOnInit() {
  }

}
