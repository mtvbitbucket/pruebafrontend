import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MailtemplateTableComponent } from './mailtemplate-table.component';

describe('MailtemplateTableComponent', () => {
  let component: MailtemplateTableComponent;
  let fixture: ComponentFixture<MailtemplateTableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MailtemplateTableComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MailtemplateTableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
