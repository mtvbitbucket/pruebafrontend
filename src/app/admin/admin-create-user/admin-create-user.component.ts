import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import { CreateUser } from "../../Shared/Models/CreateUser";



@Component({
  selector: 'app-admin-create-user',
  templateUrl: './admin-create-user.component.html',
  styleUrls: ['./admin-create-user.component.css']
})
export class AdminCreateUserComponent implements OnInit {

  @Output()
  user = new EventEmitter<CreateUser>();

  model: any = {};

  constructor() {
  }

  ngOnInit() {
  }

  createUser() {
    this.user.emit(this.model);
  }


}
