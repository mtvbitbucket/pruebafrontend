import { Component, OnInit } from '@angular/core';
import {UserService} from "../../../Shared/Services/UserService";
import {Router} from "@angular/router";

@Component({
  selector: 'app-admin-create-user-smart',
  templateUrl: './admin-create-user-smart.component.html',
  styleUrls: ['./admin-create-user-smart.component.css']
})
export class AdminCreateUserSmartComponent implements OnInit {

  model: any = {};

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  async createUser(event) {
    await this.userService.createUser(event)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/admin/users']);
        },
        error => {
          console.log(error);
        });
  }
}
