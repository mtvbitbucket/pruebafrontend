import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminCreateUserSmartComponent } from './admin-create-user-smart.component';

describe('AdminCreateUserSmartComponent', () => {
  let component: AdminCreateUserSmartComponent;
  let fixture: ComponentFixture<AdminCreateUserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminCreateUserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminCreateUserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
