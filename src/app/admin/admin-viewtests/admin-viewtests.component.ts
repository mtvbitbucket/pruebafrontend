import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Test} from "../../Shared/Models/test";

@Component({
  selector: 'app-admin-viewtests',
  templateUrl: './admin-viewtests.component.html',
  styleUrls: ['./admin-viewtests.component.css']
})
export class AdminViewtestsComponent implements OnInit {

  @Input()
  tests: Test[];

  @Input()
  simplified: boolean;

  constructor() { }

  ngOnInit() {
  }

}
