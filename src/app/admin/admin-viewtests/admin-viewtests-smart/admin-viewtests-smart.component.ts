import { Component, OnInit } from '@angular/core';
import {Test} from "../../../Shared/Models/test";
import {TestService} from "../../../Shared/Services/test.service";

@Component({
  selector: 'app-admin-viewtests-smart',
  templateUrl: './admin-viewtests-smart.component.html',
  styleUrls: ['./admin-viewtests-smart.component.css']
})
export class AdminViewtestsSmartComponent implements OnInit {

  private tests: Test[];

  private simplified: boolean;

  constructor(private testService: TestService) { }

  ngOnInit() {
    this.retrieveTests();
  }

  async retrieveTests() {
    await this.testService.getTestsAdmin().subscribe(tests => {
      this.tests = tests;
    })
  }


}
