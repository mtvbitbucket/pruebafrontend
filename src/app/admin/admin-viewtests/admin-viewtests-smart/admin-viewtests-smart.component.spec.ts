import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewtestsSmartComponent } from './admin-viewtests-smart.component';

describe('AdminViewtestsSmartComponent', () => {
  let component: AdminViewtestsSmartComponent;
  let fixture: ComponentFixture<AdminViewtestsSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewtestsSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewtestsSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
