import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminViewtestsComponent } from './admin-viewtests.component';

describe('AdminViewtestsComponent', () => {
  let component: AdminViewtestsComponent;
  let fixture: ComponentFixture<AdminViewtestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminViewtestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminViewtestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
