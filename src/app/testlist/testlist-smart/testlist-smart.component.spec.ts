import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestlistSmartComponent } from './testlist-smart.component';

describe('TestlistSmartComponent', () => {
  let component: TestlistSmartComponent;
  let fixture: ComponentFixture<TestlistSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestlistSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestlistSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
