import {Component, Input, OnInit} from '@angular/core';
import { Test } from '../../Shared/Models/test';
import {TestService} from "../../Shared/Services/test.service";
import {UserService} from "../../Shared/Services/UserService";
import {User} from "../../Shared/Models/User";

@Component({
  selector: 'app-testlist-smart',
  templateUrl: './testlist-smart.component.html',
  styleUrls: ['./testlist-smart.component.css']
})
export class TestlistSmartComponent implements OnInit {

  @Input()
  simplified: boolean;

  @Input()
  tests: Test[];

  private retailer: any = {};

  private template: any = {};

  constructor(private testService: TestService, private userService: UserService) {

  }

  ngOnInit() {
    //this.getTemplate();
    //this.getRetailer();
  }

  getTemplate(id: number) {
    this.testService.getTemplate(id).subscribe(template => this.template = template);
  }

  getRetailer(id: string) {
    this.userService.getUser(id).subscribe(users => this.retailer = users);
  }
}
