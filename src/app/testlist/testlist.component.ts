import {Component, Input, OnInit} from '@angular/core';
import {Test} from "../Shared/Models/test";

@Component({
  selector: 'app-testlist',
  templateUrl: './testlist.component.html',
  styleUrls: ['./testlist.component.css']
})
export class TestlistComponent implements OnInit {

  @Input()
  simplified: boolean;
  @Input()
  testList: Test[];

  constructor() { }

  ngOnInit() {
  }

}
