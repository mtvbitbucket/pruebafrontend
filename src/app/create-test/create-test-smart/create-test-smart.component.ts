import { Component, OnInit } from '@angular/core';
import {TestService} from "../../Shared/Services/test.service";
import {Router} from "@angular/router";
import {Template} from "../../Shared/Models/template";
import {UserService} from "../../Shared/Services/UserService";
import {User} from "../../Shared/Models/User";

@Component({
  selector: 'app-create-test-smart',
  templateUrl: './create-test-smart.component.html',
  styleUrls: ['./create-test-smart.component.css']
})
export class CreateTestSmartComponent implements OnInit {

  templates: Template[];
  users: User[];

  constructor(private testService: TestService, private router: Router, private userService: UserService) {

  }

  ngOnInit() {
    this.getTemplates();
    this.getCustomers();
  }

  getCustomers() {
    this.userService.getUsers().subscribe(users => this.users = users);
  }

  getTemplates() {
    this.testService.getTemplates().subscribe(templates => this.templates = templates);
  }

  createTest(event) {
    console.log(event);
    this.testService.createTest(event)
      .subscribe(
        data => {
          console.log(data);
          console.log("Above is nice data for your viewing pleasure!")
          this.router.navigate(['/admin/tests']);
        },
        error => {
          console.log(error);
        });
  }
}
