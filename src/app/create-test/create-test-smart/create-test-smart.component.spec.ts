import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CreateTestSmartComponent } from './create-test-smart.component';

describe('CreateTestSmartComponent', () => {
  let component: CreateTestSmartComponent;
  let fixture: ComponentFixture<CreateTestSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CreateTestSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateTestSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
