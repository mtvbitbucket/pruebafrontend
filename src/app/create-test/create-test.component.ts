import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../Shared/Models/User";
import {Template} from "../Shared/Models/template";
import {CreateTest} from "../Shared/Models/test";
import {Recipient} from "../Shared/Models/recipient";

@Component({
  selector: 'app-create-test',
  templateUrl: './create-test.component.html',
  styleUrls: ['./create-test.component.css']
})
export class CreateTestComponent implements OnInit {

  @Input()
  userList: User[];

  @Input()
  templateList: Template[];

  @Output()
  createTest = new EventEmitter();

  model: CreateTest;

  CsvData: any = {};

  file: File;

  constructor() {
    this.model = new CreateTest;
  }

  ngOnInit() {

  }

  makeTest() {
    this.createTest.emit(this.model);

  }

  changeListener(fileInput: any) {
    this.file = fileInput.target.files[0];

    let reader: FileReader = new FileReader();
    reader.readAsText(this.file);

    reader.onload = (e) => {
      let csv: string = reader.result;
      let allTextLines = csv.split(/\r|\n|\r/);
      let headers = allTextLines[0].split(';');
      let lines = [];

      for (let i = 0; i < allTextLines.length; i++) {
        //Split content at semicolons
        let data = allTextLines[i].split(',');
        if (data.length === headers.length) {
          let tarr = [];
          for (let j = 0; j < headers.length; j++) {
            tarr.push(data[j]);
          }
          lines.push(tarr);
        }
      }
      this.CsvData = lines;
      this.model.recepients = [];
      const recipients: any = [];
      for (let i = 0; i < this.CsvData.length; i++) {
        const data = this.CsvData[i];
        let recipient: Recipient = new Recipient;

        recipient.name = data[0];
        recipient.email = data[1];

        recipients.push(recipient);
      }
        this.model.recepients = recipients;
        console.log(this.model);
      };
    }
}
