/**
 * Created by Morten on 29-11-2017.
 */

/**
 * API URIs - global constants
 */
export class ApiURIs {
  private static API_IP = "https://localhost:44336";
  private static BASE_ENDPOINT = ApiURIs.API_IP.concat("/api");

  public static AUTHENTICATE = ApiURIs.API_IP.concat("/oauth/token");
  public static ACCOUNTS = ApiURIs.BASE_ENDPOINT.concat("/accounts");
  public static TESTS = ApiURIs.BASE_ENDPOINT.concat("/tests");
  public static ROLES = ApiURIs.BASE_ENDPOINT.concat("/roles");
  public static TEMPLATES = ApiURIs.BASE_ENDPOINT.concat("/templates");
}

