/**
 * Created by Morten on 29-11-2017.
 */
import { Injectable } from '@angular/core';
import { Router, CanActivate } from '@angular/router';
import {AuthenticationService} from "../Services/AuthenticationService";
import {Observable} from "rxjs";

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthenticationService) { }

  canActivate(): Observable<boolean> {
    // check with authservice if user is logged in
    if (this.auth.isLoggedIn()) {
      return Observable.of(true);
    }
    // not logged in so redirect to login page
    this.router.navigate(['/login']);
    return Observable.of(false);
  }
}
