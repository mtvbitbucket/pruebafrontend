import {CanActivate, Router} from '@angular/router';
import {AuthenticationService} from '../Services/AuthenticationService';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
/**
 * Created by Morten on 06-12-2017.
 */

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(private router: Router, private auth: AuthenticationService) {

  }

  canActivate() {
    // check if user is logged in

    if (this.auth.isLoggedIn()) {
      return this.auth.isAdmin().do(data => {
        return true;
      }, error => {
        return false;
      });
    }
    return false;
  }
}
