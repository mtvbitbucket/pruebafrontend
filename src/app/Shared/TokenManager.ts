import {StorageKeys} from './Storage/storage.keys';
import {StorageManager} from './Storage/StorageManager';
import {Injectable} from '@angular/core';
import {JwtHelper} from 'angular2-jwt';
/**
 * Created by Morten on 05-12-2017.
 */

@Injectable()

export class TokenManager {

  getToken(): string {
    if (this.tokenAvailable()) {
      return StorageManager.getStorageItem(StorageKeys.ACCESS_TOKEN);
    } else {
      return '';
    }
  }

  clearToken() {
    StorageManager.clearStorage();
  }

  tokenAvailable(): boolean {
    if (StorageManager.getStorageItem(StorageKeys.ACCESS_TOKEN)){
      return true;
    }
    return false;
  }

  tokenIsExpired(): boolean {
    const expiration = Number(StorageManager.getStorageItem(StorageKeys.EXPIRES_IN));
    const loginMilliSec = Number(StorageManager.getStorageItem(StorageKeys.LOGIN_DATE));
    const loginDate = new Date(loginMilliSec);
    const expirationDate = new Date();
    expirationDate.setTime(loginDate.getTime() + (expiration * 1000));

    if (new Date().getTime() > expirationDate.getTime())
    {
      console.log('Expired');
      return true;
    }
    console.log('Not expired');
    return false;
  }

  getDecodedToken(): string {
    if (this.tokenAvailable()) {
      const helper = new JwtHelper();
      return helper.urlBase64Decode(this.getToken());
    }
  }
}
