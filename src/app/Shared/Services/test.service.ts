import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import {Test, UpdateTest} from "../Models/test";
import {ApiURIs} from "../Util/ApiURIs";
import {Template} from "../Models/template";

@Injectable()

export class TestService {
    constructor(
        private http: HttpClient,
    ) { }

    getTestsAdmin(): Observable<Test[]> {
    return this.http.get<Test[]>(ApiURIs.TESTS.concat('/asAdmin/all'));
    }

    getTests(): Observable<Test[]> {
      return this.http.get<Test[]>(ApiURIs.TESTS.concat('/all'));
    }

    getTest(id: number): Observable<Test> {
      return this.http.get<Test>(ApiURIs.TESTS.concat('/asAdmin/single/' + id))
    }

    getTemplates(): Observable<Template[]> {
      return this.http.get<Template[]>(ApiURIs.TEMPLATES.concat('/all'))
        .map(data => {
          return data;
        },
        err => {
          throw err;
        });
    }

    getTemplate(id: number): Observable<Template> {
      return this.http.get<Template>(ApiURIs.TEMPLATES.concat('/single/' + id))
    }

  createTest(model): Observable<Test> {
    let body = model;
    console.log(model);
    return this.http.post<Test>(ApiURIs.TESTS.concat('/create'), body)
      .map(
        data => {
          return data;
        },
        err => {
          return err;
        })
  }

  editTest(model: UpdateTest): Observable<UpdateTest> {
    let body = model;

    return this.http.put<UpdateTest>(ApiURIs.TESTS.concat('/update/' + body.testid), body)
      .map(
        data => {
          console.log("This is data");
          console.log(data);
          return data;
        },
        err => {
          console.log(err);
          return err;
        });
  }

  startTheTest(id: number) {
      console.log("the test with id:" + id + " has been started");
    return this.http.get<string>(ApiURIs.TESTS.concat('/start/' + id))
      .map(
        data => {
          console.log(data);
        },
        err => {
          console.log(err);
        }
      );
  }

  stopTheTest(id: number) {
      console.log("The test with id: " + " has been stopped");
    return this.http.get(ApiURIs.TESTS.concat('/stop/' + id));
  }
}
