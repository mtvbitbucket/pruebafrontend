/**
 * Created by Morten on 29-11-2017.
 */
import { Injectable } from '@angular/core';
import {Http, Headers, Response, RequestOptions} from '@angular/http';
//noinspection TsLint
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import {ApiURIs} from '../Util/ApiURIs';
import {HttpHeaders} from '@angular/common/http';
import {HttpClient} from '@angular/common/http';
import * as moment from 'moment';
import {Token} from '../Models/token';
import {StorageManager} from '../Storage/StorageManager';
import {StorageKeys} from '../Storage/storage.keys';
import {TokenManager} from '../TokenManager';
import {Error} from 'tslint/lib/error';
import {HttpErrorResponse} from '@angular/common/http';
import {Router} from "@angular/router";



@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient, private tokenManager: TokenManager, private router: Router) {
    if (this.tokenManager.tokenIsExpired()) {
      this.tokenManager.clearToken();
    }
  }

  login(usernameInput: string, passwordInput: string): Observable<boolean> {

    // Define body of HTTP request
    let body = 'username=' + usernameInput + '&password=' + passwordInput + '&grant_type=password';

    // Define headers
    let headers = new HttpHeaders()
      .set('Content-Type', 'application/x-www-form-urlencoded');

    return this.http.post(ApiURIs.AUTHENTICATE, body,
      {
        headers: headers
      }).map(
      data => {
        let token = new Token().deserialize(data);
        StorageManager.storeItem(StorageKeys.ACCESS_TOKEN, token.access_token);
        StorageManager.storeItem(StorageKeys.EXPIRES_IN, "3600");
        StorageManager.storeItem(StorageKeys.LOGIN_DATE, new Date().getTime() + "");
        return true;
      },
      err => {
        return false;
      }
    );
  }

  public getToken() {
    return this.tokenManager.getToken();
  }

  isAdmin(): Observable<boolean> {

    const uri = ApiURIs.ROLES.concat('/isAdmin');
    //noinspection TypeScriptUnresolvedFunction
    return this.http.get(uri).map(data => {
      return true;
    }).catch((error: any) =>
      Observable.throw(false)
    );
  }



  isLoggedIn(): Observable<boolean> {
    // checks if token is available

    if (this.tokenManager.tokenAvailable()) {
      // checks if token is expired

      if (!this.tokenManager.tokenIsExpired()){
        // return true if available token is not expired
        return Observable.of(true);
      }
    }
    // return false if token is not available or expired
    return Observable.of(false);
  }

  logout(): void {
    // clear localstorage to logout user
    StorageManager.clearStorage();
    this.router.navigate(['/'])
  }
}
