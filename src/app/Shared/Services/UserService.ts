/**
 * Created by Morten on 29-11-2017.
 */
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/Rx';
import {ApiURIs} from '../Util/ApiURIs';
import {HttpClient} from '@angular/common/http';
import {HttpHeaders} from '@angular/common/http';
import {User} from '../Models/User';
import {CreateUser} from "../Models/CreateUser";


@Injectable()
export class UserService {
  constructor(private http: HttpClient) {
  }
  getUsers(): Observable<User[]> {
    return this.http.get<User[]>(ApiURIs.ACCOUNTS.concat('/all'));
  }

  getMyUsers(): Observable<User[]> {
    return this.http.get<User[]>(ApiURIs.ACCOUNTS.concat('/mine'));
  }

  createUser(model: CreateUser): Observable<CreateUser> {
    let body = model;

    return this.http.post<CreateUser>(ApiURIs.ACCOUNTS.concat('/create'), body)
      .map(
        data => {
          return data;
        },
        err => {
          return err;
        })
  }

  assignRoles(id: string, model: string[]): Observable<boolean> {
    let body = model;
    console.log(body);
    return this.http.put<string[]>(ApiURIs.ACCOUNTS.concat('/user/' + id + '/roles'), body)
      .map(
        data => {
          console.log(data);
          return true;
        },
        err => {
          console.log(err);
          return false;
        })
  }

  getUser(id: string): Observable<User> {
    return this.http.get<User>(ApiURIs.ACCOUNTS.concat('/single/' + id));
  }

  deleteUser(id: string): Observable<boolean> {
    return this.http.delete(ApiURIs.ACCOUNTS.concat('/user/' + id)).map(data => {
      return true
    },
    error => {
      return false
    });
  }

  editUser(model: User): Observable<User> {
    let body = model;

    return this.http.put<User>(ApiURIs.ACCOUNTS.concat('/user/editasadmin/' + body.id), body)
      .map(
        data => {
          console.log("This is data");
          console.log(data);
          return data;
        },
        err => {
          console.log(err);
          return err;
        });
  }

  editUserRetailer(model: User): Observable<User> {
    let body = model;

    return this.http.put<User>(ApiURIs.ACCOUNTS.concat('/user/edit/' + body.id), body)
      .map(
        data => {
          console.log("This is data");
          console.log(data);
          return data;
        },
        err => {
          console.log(err);
          return err;
        });
  }
}
