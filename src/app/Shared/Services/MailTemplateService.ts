import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {ApiURIs} from '../Util/ApiURIs';
import {Observable} from '../../../../node_modules/rxjs';
import {MailTemplate} from '../Models/MailTemplate';
import {map} from 'rxjs/operator/map';

@Injectable()
export class MailTemplateService {
  constructor(private http: HttpClient) {

  }

  getAll(): Observable<MailTemplate[]> {
      return this.http.get<MailTemplate[]>(ApiURIs.TEMPLATES.concat('/all'));
  }

  get(id: number): Observable<MailTemplate> {
    return this.http.get<MailTemplate>(ApiURIs.TEMPLATES.concat('/single/' + id));
  }

  create(model: MailTemplate): Observable<MailTemplate> {
    const body = model;
    return this.http.post<MailTemplate>(ApiURIs.TEMPLATES.concat('/create'), body).map(data => {
      return data;
    }, error => {
      return error;
    });
  }

  edit(model: MailTemplate): Observable<MailTemplate> {
    const body = model;
    return this.http.put<MailTemplate>(ApiURIs.TEMPLATES.concat('/update/' + body.id), body).map(data => {
      return data;
    }, error => {
      return error;
    });
  }

  delete(id: number): Observable<MailTemplate> {
    return this.http.delete<MailTemplate>(ApiURIs.TEMPLATES.concat('/delete/' + id));
  }
}
