import {Injectable} from "@angular/core";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {ApiURIs} from "../Util/ApiURIs";
import {Roles} from "../Models/roles";
/**
 * Created by Nicolai on 18-12-2017.
 */

@Injectable()
export class RoleService {
  constructor(private http: HttpClient) {
  }

  getRoles(): Observable<Roles[]> {
    return this.http.get<Roles[]>(ApiURIs.ROLES.concat('/all'));
  }
}
