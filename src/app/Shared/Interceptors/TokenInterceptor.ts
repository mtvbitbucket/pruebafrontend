/**
 * Created by Morten on 29-11-2017.
 */

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import {AuthenticationService} from "../Services/AuthenticationService";
import {UserService} from "../Services/UserService";
import {HttpHeaders} from "@angular/common/http";
import {TokenManager} from "../TokenManager";
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(private tokenManager: TokenManager) {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    //Get token from manager
    let accessToken = this.tokenManager.getToken();
    //check if token is null or nothinng
    if(accessToken || accessToken != "") {
      //if token then clone request and append authorization header with bearer token
      request = request.clone({headers: request.headers.append("Authorization", "Bearer " + accessToken)});
    }

    return next.handle(request);
    }
}
