/**
 * Created by Morten on 29-11-2017.
 */

import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';

import { Observable } from 'rxjs/Observable';
import {AuthenticationService} from '../Services/AuthenticationService';
import {UserService} from '../Services/UserService';
import {HttpHeaders} from '@angular/common/http';
@Injectable()
export class RequestInterceptor implements HttpInterceptor {
  constructor() {

  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    let isLoginRequest = false; /*variable for checking if request is loginrequest*/
    const requestHeaders = request.headers; /*
     get headers

     check if headers has header of 'Content-Type'. set to "" if false, set to first header of 'Content-Type' if true
     */
    const header = (!requestHeaders.has('Content-Type')) ? '' : requestHeaders.getAll('Content-Type')[0];

    /*if the fetched header is a www-header, then set loginrequest to true*/
    if (header === 'application/x-www-form-urlencoded') {
      isLoginRequest = true;
    }

    /*If not a loginrequest append application/json headers and clone request*/
    if (!isLoginRequest) {
      const headers = requestHeaders
        .append('Content-Type', 'application/json')
        .append('Accept', 'application/json');
      request = request.clone({headers: headers});
    }
    return next.handle(request);
  }


}
