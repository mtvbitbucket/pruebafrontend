/**
 * Created by Morten on 05-12-2017.
 */
export class StorageKeys {

  public static ACCESS_TOKEN = "access_token";
  public static EXPIRES_IN = "expires_in";
  public static LOGIN_DATE = "login_date";

  public static getKeys(): string[] {
    return [this.ACCESS_TOKEN, this.EXPIRES_IN, this.LOGIN_DATE];
  }
}
