import {StorageKeys} from "./storage.keys";
/**
 * Created by Morten on 05-12-2017.
 */

export class StorageManager {

  public static getStorageItem(key: string): string {
    return localStorage.getItem(key);
  }

  public static storeItem(key: string, item: string) {
    localStorage.setItem(key, item);
  }

  public static clearStorage() {
    localStorage.clear();
  }
}
