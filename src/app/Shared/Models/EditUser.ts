/**
 * Created by Nicolai on 13-12-2017.
 */

export class EditUser {
  Id: string;
  FirstName: string;
  LastName: string;
  Email: string;
  Username: string;
  Company: string;
}
