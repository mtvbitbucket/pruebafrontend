export class MailTemplate{
  id: number;
  senderDomain: string;
  senderDomainName: string;
  htmlContent: string;
  subject: string;
  redirectUrl: string;
}
