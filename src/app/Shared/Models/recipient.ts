/**
 * Created by Nicolai on 19-12-2017.
 */

export class Recipient {
  name: string;
  email: string;
  clickcount: number;
  opencount: number;
}
