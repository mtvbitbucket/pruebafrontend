import {Serializable} from "./ISerializable";
/**
 * Created by Morten on 30-11-2017.
 */
export class Token implements Serializable<Token> {
  access_token: string;
  token_type: string;
  expires_in: string;

  deserialize(input) {
    this.access_token = input.access_token;
    this.token_type = input.token_type;
    this.expires_in = input.expires_in;

    return this;
  }

}
