/**
 * Created by Morten on 29-11-2017.
 */

export class User {
  url: string;
  id: string;
  userName: string;
  firstName: string;
  lastName: string;
  email: string;
  emailConfirmed: boolean;
  company: string;
  joinDate: Date;
  roles: string[];
  retailerid: string;
}
