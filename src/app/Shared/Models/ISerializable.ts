/**
 * Created by Morten on 30-11-2017.
 */
export interface Serializable<T> {
  deserialize(input: Object): T;
}
