import {Recipient} from "./recipient";
import {User} from "./User";

export class Test {
  id: number;
  createDate: string;
  endDate: string;
  startDate: string;
  testName: string;
  customers: Array<User>;
  templateid: number;
  recepients: Array<Recipient>;
}

export class CreateTest {
  testName: string;
  customerids: Array<string>;
  templateid: number;
  recepients: Array<Recipient>;
}

export class UpdateTest {
  testid: number;
  testName: string;
  customerids: Array<string>;
  templateid: number;
  recepients: Array<Recipient>;
}
