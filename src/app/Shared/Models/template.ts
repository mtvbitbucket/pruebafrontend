/**
 * Created by Nicolai on 15-12-2017.
 */

export class Template {
  redirectUrl: string;
  id: number;
  senderDomain: string;
  senderDomainName: string;
  htmlContent: string;
  subject: string;
}
