/**
 * Created by Nicolai on 09-12-2017.
 */

export class CreateUser {
  Email: string;
  UserName: string;
  Password: string;
  ConfirmPassword: string;
  FirstName: string;
  LastName: string;
  Company: string;
  RetailerId: string;
}

