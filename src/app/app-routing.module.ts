import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./login/login.component";
import {AuthGuard} from "./Shared/Guards/AuthGuard";
import {AdminGuard} from "./Shared/Guards/AdminGuard";
import {LandingPageComponent} from "./landing-page/landing-page.component";
import {AdminUpdateuserSmartComponent} from "./admin/admin-updateuser/admin-updateuser-smart/admin-updateuser-smart.component";
import {AdminDeleteUserSmartComponent} from "./admin/admin-delete-user/admin-delete-user-smart/admin-delete-user-smart.component";
import {AdminViewuserSmartComponent} from "./admin/admin-viewuser/admin-viewuser-smart/admin-viewuser-smart.component";
import {CreateTestSmartComponent} from "./create-test/create-test-smart/create-test-smart.component";
import {AdminViewtestsSmartComponent} from "./admin/admin-viewtests/admin-viewtests-smart/admin-viewtests-smart.component";
import {AdminAssignrolesSmartComponent} from "./admin/admin-assignroles/admin-assignroles-smart/admin-assignroles-smart.component";
import {AdminSmartComponent} from "./admin/admin-smart/admin-smart.component";
import {RetailerCreateuserSmartComponent} from "./retailer/retailer-createuser/retailer-createuser-smart/retailer-createuser-smart.component";
import {RetailerSmartComponent} from "./retailer/retailer-smart/retailer-smart.component";
import {RetailerUpdateuserSmartComponent} from "./retailer/retailer-updateuser/retailer-updateuser-smart/retailer-updateuser-smart.component";
import {RetailerViewuserSmartComponent} from "./retailer/retailer-viewuser/retailer-viewuser-smart/retailer-viewuser-smart.component";
import {RetailerViewtestsSmartComponent} from "./retailer/retailer-viewtests/retailer-viewtests-smart/retailer-viewtests-smart.component";
import {AdminViewTemplatesSmartComponent} from './admin/admin-view-templates/admin-view-templates-smart/admin-view-templates-smart.component';
import {AdminCreateTemplateSmartComponent} from './admin/admin-create-template/admin-create-template-smart/admin-create-template-smart.component';
import {AdminEditTemplateSmartComponent} from './admin/admin-edit-template/admin-edit-template-smart/admin-edit-template-smart.component';
import {ViewTestSmartComponent} from "./view-test/view-test-smart/view-test-smart.component";
import {AdminCreateUserSmartComponent} from "./admin/admin-create-user/admin-create-user-smart/admin-create-user-smart.component";
import {EditTestSmartComponent} from "./edit-test/edit-test-smart/edit-test-smart.component";

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'retailer', component: RetailerSmartComponent, canActivate: [AuthGuard] },
  { path: 'retailer/user/create', component: RetailerCreateuserSmartComponent, canActivate: [AuthGuard] },
  { path: 'retailer/user/edit/:id', component: RetailerUpdateuserSmartComponent, canActivate: [AuthGuard] },
  { path: 'retailer/users', component: RetailerViewuserSmartComponent, canActivate: [AuthGuard] },
  { path: 'retailer/tests', component: RetailerViewtestsSmartComponent, canActivate: [AuthGuard] },
  { path: 'admin', component: AdminSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/create', component: AdminCreateUserSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/edit/:id', component: AdminUpdateuserSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/delete/:id', component: AdminDeleteUserSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/assignroles/:id', component: AdminAssignrolesSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/users', component: AdminViewuserSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/tests', component: AdminViewtestsSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/templates', component: AdminViewTemplatesSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/templates/create', component: AdminCreateTemplateSmartComponent, canActivate: [AdminGuard] },
  { path: 'admin/templates/edit/:id', component: AdminEditTemplateSmartComponent, canActivate: [AdminGuard] },
  { path: 'test/create', component: CreateTestSmartComponent, canActivate: [AuthGuard] },
  { path: 'test/:id', component: ViewTestSmartComponent, canActivate: [AuthGuard] },
  //{ path: 'test/edit/:id', component: EditTestSmartComponent, canActivate: [AuthGuard] },
  { path: '', component: LandingPageComponent },
  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
