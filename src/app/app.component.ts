import {Component, OnInit} from '@angular/core';
import {Observable} from "rxjs";
import {AuthenticationService} from "./Shared/Services/AuthenticationService";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})



export class AppComponent implements OnInit {
  title = 'app';

  adminActivate: boolean;
  allActivate: boolean;
  dashboardRoute: string;
  userListRoute: string;
  createUserRoute: string;
  testListRoute: string;
  createTestRoute: string;

  constructor(private authService: AuthenticationService) {
    this.authService.isLoggedIn().subscribe(data => this.allActivate = data, err => console.log(err));
    if(this.allActivate)
    {
    this.setRoutes();
    this.setAdminRoutes();
    }
  }

  ngOnInit() {

  }


  setRoutes() {
    this.dashboardRoute = '/retailer';
    this.userListRoute = '/retailer/users';
    this.testListRoute = '/retailer/tests';
    this.createUserRoute = '/retailer/user/create';
    this.createTestRoute = '/test/create';
  }

  setAdminRoutes() {
    this.authService.isAdmin().
    subscribe(data => {
        this.adminActivate = data;
        this.dashboardRoute = '/admin';
        this.userListRoute = '/admin/users';
        this.createUserRoute = '/admin/create';
        this.testListRoute = '/admin/tests';
        this.createTestRoute = '/test/create';
      },
      err => {
        console.log(err)
      });
  }

  logout() {
    this.authService.logout();
    window.location.reload();
  }
}
