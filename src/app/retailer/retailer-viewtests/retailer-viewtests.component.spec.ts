import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerViewtestsComponent } from './retailer-viewtests.component';

describe('RetailerViewtestsComponent', () => {
  let component: RetailerViewtestsComponent;
  let fixture: ComponentFixture<RetailerViewtestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerViewtestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerViewtestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
