import { Component, OnInit } from '@angular/core';
import {TestService} from "../../../Shared/Services/test.service";
import {Test} from "../../../Shared/Models/test";

@Component({
  selector: 'app-retailer-viewtests-smart',
  templateUrl: './retailer-viewtests-smart.component.html',
  styleUrls: ['./retailer-viewtests-smart.component.css']
})
export class RetailerViewtestsSmartComponent implements OnInit {

  private tests: Test[];

  private simplified: boolean;

  constructor(private testService: TestService) { }

  ngOnInit() {
    this.simplified = false;
    this.retrieveTests();
  }

  async retrieveTests() {
    await this.testService.getTests().subscribe(tests => {
      this.tests = tests;
    })
  }

}
