import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerViewtestsSmartComponent } from './retailer-viewtests-smart.component';

describe('RetailerViewtestsSmartComponent', () => {
  let component: RetailerViewtestsSmartComponent;
  let fixture: ComponentFixture<RetailerViewtestsSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerViewtestsSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerViewtestsSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
