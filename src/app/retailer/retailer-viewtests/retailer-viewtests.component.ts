import {Component, Input, OnInit} from '@angular/core';
import {Test} from "../../Shared/Models/test";

@Component({
  selector: 'app-retailer-viewtests',
  templateUrl: './retailer-viewtests.component.html',
  styleUrls: ['./retailer-viewtests.component.css']
})
export class RetailerViewtestsComponent implements OnInit {

  @Input()
  tests: Test[];

  @Input()
  simplified: boolean;

  constructor() { }

  ngOnInit() {
  }

}
