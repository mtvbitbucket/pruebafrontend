import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CreateUser} from "../../Shared/Models/CreateUser";

@Component({
  selector: 'app-retailer-createuser',
  templateUrl: './retailer-createuser.component.html',
  styleUrls: ['./retailer-createuser.component.css']
})
export class RetailerCreateuserComponent implements OnInit {

  @Output()
  user = new EventEmitter<CreateUser>();

  model: any = {};

  constructor() {
  }

  ngOnInit() {
  }

  createUser() {
    this.user.emit(this.model);
  }

}
