import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {UserService} from "../../../Shared/Services/UserService";

@Component({
  selector: 'app-retailer-createuser-smart',
  templateUrl: './retailer-createuser-smart.component.html',
  styleUrls: ['./retailer-createuser-smart.component.css']
})
export class RetailerCreateuserSmartComponent implements OnInit {

  constructor(private userService: UserService, private router: Router) { }

  ngOnInit() {
  }

  async createUser(event) {
    await this.userService.createUser(event)
      .subscribe(
        data => {
          console.log(data);
          this.router.navigate(['/admin/users']);
        },
        error => {
          console.log(error);
        });
  }
}
