import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerCreateuserSmartComponent } from './retailer-createuser-smart.component';

describe('RetailerCreateuserSmartComponent', () => {
  let component: RetailerCreateuserSmartComponent;
  let fixture: ComponentFixture<RetailerCreateuserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerCreateuserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerCreateuserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
