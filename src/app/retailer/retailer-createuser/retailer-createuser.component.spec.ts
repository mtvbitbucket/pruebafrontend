import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerCreateuserComponent } from './retailer-createuser.component';

describe('RetailerCreateuserComponent', () => {
  let component: RetailerCreateuserComponent;
  let fixture: ComponentFixture<RetailerCreateuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerCreateuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerCreateuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
