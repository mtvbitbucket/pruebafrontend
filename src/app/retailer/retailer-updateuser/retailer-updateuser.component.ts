import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {User} from "../../Shared/Models/User";
import {EditUser} from "../../Shared/Models/EditUser";

@Component({
  selector: 'app-retailer-updateuser',
  templateUrl: './retailer-updateuser.component.html',
  styleUrls: ['./retailer-updateuser.component.css']
})
export class RetailerUpdateuserComponent implements OnInit {

  @Input()
  user: User;

  @Output()
  updatedUser = new EventEmitter<EditUser>();

  model: any = {};

  constructor() {

  }

  ngOnInit() {
    if (this.user) {
      this.model = this.user;
    }
  }

  updateUser() {
    this.updatedUser.emit(this.model);
  }

}
