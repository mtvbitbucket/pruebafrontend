import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerUpdateuserComponent } from './retailer-updateuser.component';

describe('RetailerUpdateuserComponent', () => {
  let component: RetailerUpdateuserComponent;
  let fixture: ComponentFixture<RetailerUpdateuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerUpdateuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerUpdateuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
