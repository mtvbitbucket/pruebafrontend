import { Component, OnInit } from '@angular/core';
import {User} from "../../../Shared/Models/User";
import {ActivatedRoute, Router} from "@angular/router";
import {UserService} from "../../../Shared/Services/UserService";

@Component({
  selector: 'app-retailer-updateuser-smart',
  templateUrl: './retailer-updateuser-smart.component.html',
  styleUrls: ['./retailer-updateuser-smart.component.css']
})
export class RetailerUpdateuserSmartComponent implements OnInit {

  private id: string;

  private user: User;

  private updatesUser: any = {};


  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router) {
    this.id = route.snapshot.paramMap.get('id');
    this.getUser();
  }

  async getUser() {
    await this.userService.getUser(this.id).subscribe(data => {
      this.user = data;
    })
  }

  async updateUser(event) {
    this.updatesUser = event;
    await this.userService.editUserRetailer(this.updatesUser)
      .subscribe(
        data => {
          this.router.navigate(['/retailer/users']);
          return data;
        },
        error => {
          console.log(error);
          return error;
        });
  }

  ngOnInit() {
  }

}
