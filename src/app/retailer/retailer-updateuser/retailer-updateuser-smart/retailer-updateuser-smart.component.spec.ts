import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerUpdateuserSmartComponent } from './retailer-updateuser-smart.component';

describe('RetailerUpdateuserSmartComponent', () => {
  let component: RetailerUpdateuserSmartComponent;
  let fixture: ComponentFixture<RetailerUpdateuserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerUpdateuserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerUpdateuserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
