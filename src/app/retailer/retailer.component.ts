import {Component, Input, OnInit} from '@angular/core';
import { UserService } from '../Shared/Services/UserService';
import { AuthenticationService } from '../Shared/Services/AuthenticationService';
import { User } from '../Shared/Models/User';
import {Test} from "../Shared/Models/test";

@Component({
  selector: 'app-retailer',
  templateUrl: './retailer.component.html',
  styleUrls: ['./retailer.component.css']
})
export class RetailerComponent implements OnInit {

  @Input()
  users: User[];

  @Input()
  tests: Test[];

  @Input()
  simplified: boolean;

  constructor() {
  }

  ngOnInit() {
  }

}
