import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerSmartComponent } from './retailer-smart.component';

describe('RetailerSmartComponent', () => {
  let component: RetailerSmartComponent;
  let fixture: ComponentFixture<RetailerSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
