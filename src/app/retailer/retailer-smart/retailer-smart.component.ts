import { Component, OnInit } from '@angular/core';
import {Test} from "../../Shared/Models/test";
import {User} from "../../Shared/Models/User";
import {TestService} from "../../Shared/Services/test.service";
import {UserService} from "../../Shared/Services/UserService";

@Component({
  selector: 'app-retailer-smart',
  templateUrl: './retailer-smart.component.html',
  styleUrls: ['./retailer-smart.component.css']
})
export class RetailerSmartComponent implements OnInit {

  private tests: Test[];
  private users: User[];
  private simplified: boolean;

  constructor(private testService: TestService, private userService: UserService) { }

  ngOnInit() {
    this.simplified = true;
    this.retrieveUsers();
    this.retrieveTests();
  }

  async retrieveUsers() {
    await this.userService.getMyUsers().subscribe(users => {
      this.users = users;
    })
  }

  async retrieveTests() {
    await this.testService.getTests().subscribe(tests => {
      this.tests = tests;
    })
  }

}
