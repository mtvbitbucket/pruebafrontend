import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerViewuserComponent } from './retailer-viewuser.component';

describe('RetailerViewuserComponent', () => {
  let component: RetailerViewuserComponent;
  let fixture: ComponentFixture<RetailerViewuserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerViewuserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerViewuserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
