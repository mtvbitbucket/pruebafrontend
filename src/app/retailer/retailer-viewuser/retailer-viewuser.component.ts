import {Component, Input, OnInit} from '@angular/core';
import {UserService} from "../../Shared/Services/UserService";
import {User} from "../../Shared/Models/User";

@Component({
  selector: 'app-retailer-viewuser',
  templateUrl: './retailer-viewuser.component.html',
  styleUrls: ['./retailer-viewuser.component.css']
})
export class RetailerViewuserComponent implements OnInit {

  @Input()
  simplified: boolean;

  @Input()
  users: User[];

  constructor() { }

  ngOnInit() {
  }
}
