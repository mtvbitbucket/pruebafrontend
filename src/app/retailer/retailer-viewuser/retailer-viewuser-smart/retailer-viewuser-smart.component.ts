import { Component, OnInit } from '@angular/core';
import {User} from "../../../Shared/Models/User";
import {UserService} from "../../../Shared/Services/UserService";

@Component({
  selector: 'app-retailer-viewuser-smart',
  templateUrl: './retailer-viewuser-smart.component.html',
  styleUrls: ['./retailer-viewuser-smart.component.css']
})
export class RetailerViewuserSmartComponent implements OnInit {

  private simplified: boolean;

  private users: User[];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.simplified = false;
    this.retrieveAllUsers();
  }
  async retrieveAllUsers() {
    await this.userService.getMyUsers().subscribe(users => {
      this.users = users;
    });
  }

}
