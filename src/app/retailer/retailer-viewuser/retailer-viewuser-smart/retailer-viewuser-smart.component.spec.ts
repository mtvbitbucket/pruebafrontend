import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailerViewuserSmartComponent } from './retailer-viewuser-smart.component';

describe('RetailerViewuserSmartComponent', () => {
  let component: RetailerViewuserSmartComponent;
  let fixture: ComponentFixture<RetailerViewuserSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailerViewuserSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailerViewuserSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
