import { Component, OnInit } from '@angular/core';
import {TestService} from "../../Shared/Services/test.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Template} from "../../Shared/Models/template";
import {CreateTest, Test, UpdateTest} from "../../Shared/Models/test";
import {UserService} from "../../Shared/Services/UserService";
import {User} from "../../Shared/Models/User";


@Component({
  selector: 'app-edit-test-smart',
  templateUrl: './edit-test-smart.component.html',
  styleUrls: ['./edit-test-smart.component.css']
})
export class EditTestSmartComponent implements OnInit {

  private id: number;

  test = new Test;
  templates = new Array<Template>();
  users = new Array<User>();
  newTest = new UpdateTest;

  constructor(private route: ActivatedRoute, private testService: TestService, private router: Router, private userService: UserService) {
    this.getTemplates();
    this.getCustomers();
  }

  async ngOnInit() {
    this.id = +await this.route.snapshot.paramMap.get('id');

    console.log(this.id);

    await this.testService.getTest(this.id).subscribe(data => {
        this.test = data;
        this.parseToCreateTest();
      },
      err => {
        console.log(err);
      });

  }

  async getTemplates() {
    await this.testService.getTemplates().subscribe(templates => this.templates = templates);
  }

  async getCustomers() {
    await this.userService.getMyUsers().subscribe(users => this.users = users);
  }

  async updateTest(event) {
    console.log(event);
    await this.testService.editTest(event)
      .subscribe(
        data => {
          this.router.navigate(['/admin/tests']);
          return data;
        },
        error => {
          console.log(error);
          return error;
        });
  }

  parseToCreateTest() {
    if (this.test) {
      this.newTest.testid = this.test.id;
      this.newTest.testName = this.test.testName;
      this.newTest.recepients = [];
      this.newTest.templateid = this.test.templateid;

      let customerids: any = [];

      for (let x of this.test.customers) {
        customerids.push(x.id);
      }

      this.newTest.customerids = customerids;
    }
    console.log(this.newTest);
  }

}
