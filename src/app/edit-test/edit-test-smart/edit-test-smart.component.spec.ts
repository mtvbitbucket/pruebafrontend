import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTestSmartComponent } from './edit-test-smart.component';

describe('EditTestSmartComponent', () => {
  let component: EditTestSmartComponent;
  let fixture: ComponentFixture<EditTestSmartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EditTestSmartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTestSmartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
