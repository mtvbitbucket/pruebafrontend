import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {CreateTest, Test, UpdateTest} from "../Shared/Models/test";
import {Template} from "../Shared/Models/template";
import {Recipient} from "../Shared/Models/recipient";
import {User} from "../Shared/Models/User";

@Component({
  selector: 'app-edit-test',
  templateUrl: './edit-test.component.html',
  styleUrls: ['./edit-test.component.css']
})
export class EditTestComponent implements OnInit {

  @Input()
  userList: User[];

  @Input()
  test: Test;

  @Input()
  templateList: Template[];

  @Output()
  updatedTest = new EventEmitter<UpdateTest>();

  model: any = {};

  CsvData: any = {};

  file: File;

  constructor() {

  }

  ngOnInit() {
      this.model = this.test;
  }

  updateTest() {
    console.log(this.model);
    this.updatedTest.emit(this.model);
  }

  changeListener(fileInput: any) {
    this.file = fileInput.target.files[0];

    let reader: FileReader = new FileReader();
    reader.readAsText(this.file);

    reader.onload = (e) => {
      let csv: string = reader.result;
      let allTextLines = csv.split(/\r|\n|\r/);
      let headers = allTextLines[0].split(';');
      let lines = [];

      for (let i = 0; i < allTextLines.length; i++) {
        //Split content at commas
        let data = allTextLines[i].split(',');
        if (data.length === headers.length) {
          let tarr = [];
          for (let j = 0; j < headers.length; j++) {
            tarr.push(data[j]);
          }
          lines.push(tarr);
        }
      }
      this.CsvData = lines;
      this.model.recepients = [];
      const recipients: any = [];
      for (let i = 0; i < this.CsvData.length; i++) {
        const data = this.CsvData[i];
        let recipient: Recipient = new Recipient;

        recipient.name = data[0];
        recipient.email = data[1];

        recipients.push(recipient);
      }
      this.model.recepients = recipients;
      console.log(this.model);
    };
  }
}
